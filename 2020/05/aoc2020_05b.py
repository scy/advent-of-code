import math


def read_input():
    return [line.strip() for line in open('input.txt', 'r').readlines()]


def seat_id(partitions):
    partitions = partitions.replace('B', '1').replace('R', '1')
    partitions = partitions.replace('F', '0').replace('L', '0')
    row = int(partitions[:7], 2)
    col = int(partitions[7:], 2)
    return row * 8 + col


def find_gap(partitions):
    seat_ids = [seat_id(partition) for partition in partitions]
    return [i for i in range(min(seat_ids), max(seat_ids)) if i not in seat_ids][0]


if __name__ == '__main__':
    print('your seat is {0}'.format(
        find_gap(read_input())
    ))
