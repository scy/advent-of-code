import math


def read_input():
    return [line.strip() for line in open('input.txt', 'r').readlines()]


def seat_id(partitions):
    lower, upper = 0, 127
    for partition in partitions[:7]:
        lower, upper = bounds(lower, upper, partition)
    if lower != upper:
        print('lower != upper for row {0}'.format(partitions))
    row = lower
    lower, upper = 0, 7
    for partition in partitions[7:]:
        lower, upper = bounds(lower, upper, partition)
    if lower != upper:
        print('lower != upper for col {0}'.format(partitions))
    return row * 8 + lower


def bounds(lower, upper, selection):
    border = lower + ((upper - lower) / 2)
    if selection == 'F' or selection == 'L':
        return lower, math.floor(border)
    if selection == 'B' or selection == 'R':
        return math.ceil(border), upper
    print('uhm, selection is {0}'.format(selection))


if __name__ == '__main__':
    print('highest seat id is {0}'.format(
        max([seat_id(partition) for partition in read_input()])
    ))
