def evaluate(expr):
    lvl = 0
    sub = ''
    num = result = op = None
    for char in expr:
        if lvl > 0:
            sub += char
            if char == ')':
                lvl -= 1
                if lvl == 0:
                    sub_result = evaluate(sub[1:-1])
                    if result:
                        result = (result + sub_result) if op == '+' else (result * sub_result)
                    else:
                        result = sub_result
                    sub = ''
            elif char == '(':
                lvl += 1
            continue
        # If we get to here, we are not in a subexpression.
        if char == ' ':
            continue
        elif char in '+*':
            op = char
        elif char == '(':
            lvl = 1
            sub = '('
        elif char in '0123456789':
            num = int(char)
            if op:
                result = (result + num) if op == '+' else (result * num)
            else:
                result = num
        else:
            raise ValueError('invalid character {0} in expression {1}'.format(
                char, expr))
    return result


def read_input():
    with open('input.txt', 'r') as f:
        while True:
            line = f.readline()
            if len(line) == 0:
                return
            yield line.strip()


def sum_of_results():
    sum = 0
    for expr in read_input():
        result = evaluate(expr)
        # print('{0} = {1}'.format(expr, result))
        sum += result
    return sum


if __name__ == '__main__':
    print('The sum of all of my expressions is {0}.'.format(
        sum_of_results()))
