Alle Infos: https://codeberg.org/scy/advent-of-code

Wir coden hier zusammen live die heutigen Advent-of-Code-Programmieraufgaben in Python und nem reinen Text-Terminal unter Linux. Mehr Infos zu Advent of Code selbst: https://adventofcode.com/

Den Chat findet ihr auf Matrix: https://matrix.to/#/#livestream:scy.name
Wenn ihr noch keinen Matrix-Account habt, findet ihr Infos auf https://matrix.org/ und könnt euch unter https://app.element.io/ nen kostenlosen Account klicken.
