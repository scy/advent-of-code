class TicketChecker:

    def __init__(self):
        self.rules = {}
        self.my_ticket = None
        self.nearby_tickets = []
        self.mapping = None
        read_tickets = False

        for line in self.read_input():
            # Mode switching depending on line:
            if len(line) == 0:
                continue
            if line == 'your ticket:' or line == 'nearby tickets:':
                read_tickets = True
                continue

            if read_tickets:  # We're reading ticket values.
                values = tuple(int(x) for x in line.split(','))
                if self.my_ticket is None:
                    self.my_ticket = values
                else:
                    self.nearby_tickets.append(values)
            else:  # We're reading the rules.
                field, rules = line.split(':', 2)
                intervals = rules.strip().split(' or ')
                self.rules[field] = [
                    tuple(int(x) for x in interval.split('-'))
                    for interval in intervals
                ]

    def matching_rules(self, value):
        matching = []
        for rule, intervals in self.rules.items():
            for interval in intervals:
                if value >= interval[0] and value <= interval[1]:
                    matching.append(rule)
        return matching

    def discard_bad_tickets(self):
        count = 0
        for idx in range(len(self.nearby_tickets) - 1, -1, -1):
            for value in self.nearby_tickets[idx]:
                if len(self.matching_rules(value)) == 0:
                    count += 1
                    del self.nearby_tickets[idx]
                    break
        return count

    def determine_mapping(self):
        # We start by considering every rule valid for every field.
        candidates = [set(self.rules.keys()) for _ in range(len(self.my_ticket))]

        # Then, remove those rules that are broken by at least one value in that field.
        for values in self.nearby_tickets:
            for idx, value in enumerate(values):
                matching = self.matching_rules(value)
                not_matching = [c for c in candidates[idx] if c not in matching]
                for to_remove in not_matching:
                    candidates[idx].discard(to_remove)

        # Lastly, reduce the candidates even more by looking whether one rule
        # is the only one in a certain field and remove it from all other fields.
        # Repeat that process.
        mapping = {}
        while True:
            # Find a field with only a single candidate.
            singles = [(idx, can) for (idx, can) in enumerate(candidates) if len(can) == 1]
            if len(singles) == 0:
                break
            idx, single = singles[0]
            single = single.pop()
            mapping[idx] = single
            # Remove that rule name from all of the other fields.
            for can in candidates:
                can.discard(single)
        self.mapping = [mapping[idx] for idx in range(len(self.my_ticket))]

    def get_my_mapped_ticket(self):
        if self.mapping is None:
            raise RuntimeError('no mapping determined yet')
        return {self.mapping[idx]: val for idx, val in enumerate(self.my_ticket)}

    def read_input(self):
        with open('input.txt', 'r') as f:
            while True:
                line = f.readline()
                if len(line) == 0:
                    return
                yield line.strip()


if __name__ == '__main__':
    tc = TicketChecker()
    print('Discarded {0} bad tickets.'.format(tc.discard_bad_tickets()))
    tc.determine_mapping()
    my = tc.get_my_mapped_ticket()
    print('My ticket:', my)
    product = 1
    for k, v in my.items():
        if k.startswith('departure '):
            product *= v
    print('Multiplied departure values: {0}'.format(product))
