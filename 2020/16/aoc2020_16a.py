class TicketChecker:

    def __init__(self):
        self.rules = {}
        self.my_ticket = None
        self.nearby_tickets = []
        read_tickets = False

        for line in self.read_input():
            # Mode switching depending on line:
            if len(line) == 0:
                continue
            if line == 'your ticket:' or line == 'nearby tickets:':
                read_tickets = True
                continue

            if read_tickets:  # We're reading ticket values.
                values = tuple(int(x) for x in line.split(','))
                if self.my_ticket is None:
                    self.my_ticket = values
                else:
                    self.nearby_tickets.append(values)
            else:  # We're reading the rules.
                field, rules = line.split(':', 2)
                intervals = rules.strip().split(' or ')
                self.rules[field] = [
                    tuple(int(x) for x in interval.split('-'))
                    for interval in intervals
                ]

    def matching_rules(self, value):
        matching = []
        for rule, intervals in self.rules.items():
            for interval in intervals:
                if value >= interval[0] and value <= interval[1]:
                    matching.append(rule)
        return matching

    def sum_of_bad_values(self):
        sum = 0
        tickets = 0
        for ticket in self.nearby_tickets:
            is_bad = False
            for value in ticket:
                if len(self.matching_rules(value)) == 0:
                    is_bad = True
                    sum += value
            tickets += 1 if is_bad else 0
        return sum, tickets

    def read_input(self):
        with open('input.txt', 'r') as f:
            while True:
                line = f.readline()
                if len(line) == 0:
                    return
                yield line.strip()


if __name__ == '__main__':
    tc = TicketChecker()
    print('The ticket scanning error rate is {0} ({1} bad tickets).'.format(
        *tc.sum_of_bad_values()))
