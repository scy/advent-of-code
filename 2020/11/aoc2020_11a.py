# Rules:
# If seat empty and no occupied seats adjacent -> occupied
# If set occupied and 4 or more adjacent also occupied -> empty
# Else, no change


class Ferry:

    def __init__(self):
        self.plan = [line.strip() for line in open('input.txt', 'r').readlines()]

    def get_cell(self, x, y):
        if y < 0 or y >= len(self.plan):
            return 'X'
        if x < 0 or x >= len(self.plan[y]):
            return 'X'
        return self.plan[y][x]

    def adjacent_occupied(self, x, y):
        occupied = 0
        for check_y in range(y-1, y+2):
            for check_x in range(x-1, x+2):
                if not (check_x == x and check_y == y) and \
                        self.get_cell(check_x, check_y) == '#':
                    occupied += 1
        return occupied

    def iterate(self):
        changed = False
        new_plan = []
        for y, row in enumerate(self.plan):
            new_row = ''
            for x, cell in enumerate(row):
                if cell == 'L' and self.adjacent_occupied(x, y) == 0:
                    new_row += '#'
                    changed = True
                elif cell == '#' and self.adjacent_occupied(x, y) >= 4:
                    new_row += 'L'
                    changed = True
                else:
                    new_row += cell
            new_plan.append(new_row)
        self.plan = new_plan
        return changed

    def iterate_until_no_change(self):
        while self.iterate():
            self.print_plan()
            pass

    def count_occupied(self):
        count = 0
        for row in self.plan:
            for cell in row:
                if cell == '#':
                    count += 1
        return count

    def print_plan(self):
        print('\n'.join(self.plan))
        print()


if __name__ == '__main__':
    f = Ferry()
    f.iterate_until_no_change()
    print('In the end, {0} seats are occupied.'.format(
        f.count_occupied()))
