# Rules:
# If seat empty and no occupied seats visible -> occupied
# If set occupied and 5 or more visible also occupied -> empty
# Else, no change


movement_vectors = (
    (-1, -1), (-1, 0), (-1, 1),
    ( 0, -1),          ( 0, 1),
    ( 1, -1), ( 1, 0), ( 1, 1),
)


class Ferry:

    def __init__(self):
        self.plan = [line.strip() for line in open('input.txt', 'r').readlines()]

    def get_cell(self, x, y):
        if y < 0 or y >= len(self.plan):
            return 'X'
        if x < 0 or x >= len(self.plan[y]):
            return 'X'
        return self.plan[y][x]

    def visible_occupied(self, from_x, from_y):
        occupied = 0
        for x_vec, y_vec in movement_vectors:
            x = from_x
            y = from_y
            while True:
                x += x_vec
                y += y_vec
                cell = self.get_cell(x, y)
                if cell in 'XL':
                    break
                elif cell == '#':
                    occupied += 1
                    break
        return occupied

    def iterate(self):
        changed = False
        new_plan = []
        for y, row in enumerate(self.plan):
            new_row = ''
            for x, cell in enumerate(row):
                occupied = self.visible_occupied(x, y)
                if cell == 'L' and occupied == 0:
                    new_row += '#'
                    changed = True
                elif cell == '#' and occupied >= 5:
                    new_row += 'L'
                    changed = True
                else:
                    new_row += cell
            new_plan.append(new_row)
        self.plan = new_plan
        return changed

    def iterate_until_no_change(self):
        while self.iterate():
            pass

    def count_occupied(self):
        count = 0
        for row in self.plan:
            for cell in row:
                if cell == '#':
                    count += 1
        return count

    def print_plan(self):
        print('\n'.join(self.plan))
        print()


if __name__ == '__main__':
    f = Ferry()
    f.iterate_until_no_change()
    print('In the end, {0} seats are occupied.'.format(
        f.count_occupied()))
