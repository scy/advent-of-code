class Docking:

    def __init__(self):
        self.memory = {}
        self.mask = 36 * '0'

    def read_input(self):
        with open('input.txt', 'r') as f:
            while True:
                line = f.readline()
                if len(line) == 0:
                    return
                parts = line.strip().split(' ')
                if len(parts) != 3 or parts[1] != '=':
                    raise ValueError('strange line: {0}'.format(
                        line.strip()))
                if parts[0] == 'mask':
                    yield (parts[0], parts[2])
                elif parts[0][:4] == 'mem[':
                    yield (
                        int(parts[0].split('[')[1].split(']')[0]),
                        int(parts[2]))
                else:
                    raise ValueError('unknown command: {0}'.format(
                        line.strip()))

    def expand_mask(self, mask):
        placeholder_count = mask.count('X')
        format_str = '{0:0>' + str(placeholder_count) + '}'
        for bits in range(2 ** placeholder_count):
            bits = format_str.format(bin(bits)[2:])
            target = ''
            x_pos = 0
            for bit in mask:
                if bit == 'X':
                    target += bits[x_pos]
                    x_pos += 1
                else:
                    target += bit
            yield int(target, 2)

    def run(self):
        for addr, val in self.read_input():
            if addr == 'mask':
                self.mask = val
            else:
                # Create address template from address and bitmask.
                addr = '{0:0>36}'.format(bin(addr)[2:])
                target_mask = ''
                for bit, mask in zip(addr, self.mask):
                    target_mask += bit if mask == '0' else mask

                # Write value to each of the addresses that match the template.
                for addr in self.expand_mask(target_mask):
                    self.memory[addr] = val


if __name__ == '__main__':
    d = Docking()
    d.run()
    print('Sum of all {0} memory cells is {1}.'.format(
        len(d.memory), sum(d.memory.values())))
