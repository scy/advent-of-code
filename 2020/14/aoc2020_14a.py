class Docking:

    def __init__(self):
        self.memory = {}
        self.mask = 36 * 'X'

    def read_input(self):
        with open('input.txt', 'r') as f:
            while True:
                line = f.readline()
                if len(line) == 0:
                    return
                parts = line.strip().split(' ')
                if len(parts) != 3 or parts[1] != '=':
                    raise ValueError('strange line: {0}'.format(
                        line.strip()))
                if parts[0] == 'mask':
                    yield (parts[0], parts[2])
                elif parts[0][:4] == 'mem[':
                    yield (
                        int(parts[0].split('[')[1].split(']')[0]),
                        int(parts[2]))
                else:
                    raise ValueError('unknown command: {0}'.format(
                        line.strip()))

    def run(self):
        for addr, val in self.read_input():
            if addr == 'mask':
                self.mask = val
            else:
                val = '{0:0>36}'.format(bin(val)[2:])
                bits = ''
                for bit, mask in zip(val, self.mask):
                    bits += bit if mask == 'X' else mask
                self.memory[addr] = int(bits, 2)


if __name__ == '__main__':
    d = Docking()
    d.run()
    print('Sum of all memory cells is {0}.'.format(
        sum(d.memory.values())))
