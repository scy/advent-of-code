def read_input():
    with open('input.txt', 'r') as f:
        return int(f.readline().strip()), int(f.readline().strip())


def transform(subj, loop_size, expected=None):
    val = 1
    for i in range(loop_size):
        val *= subj
        val %= 20201227
        if val == expected:
            return i + 1  # because range() starts at 0
    return val if expected is None else None


def bruteforce(keys, max_loop_size=100_000_000):
    return tuple(transform(7, max_loop_size, pub) for pub in keys)


if __name__ == '__main__':
    pub = read_input()
    print('Brute-forcing keys...')
    prv = bruteforce(pub)
    print('Private keys are: {0}'.format(prv))
    print('Calculating encryption key...')
    print('The encryption key is: {0}'.format(
        transform(pub[0], prv[1])))
