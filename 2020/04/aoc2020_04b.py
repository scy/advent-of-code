import re


YEAR = re.compile('^[0-9]{4}$')
HEIGHT = re.compile('^[0-9]{2,3}(cm|in)$')
HEX_COLOR = re.compile('^#[0-9a-f]{6}$')
PID = re.compile('^[0-9]{9}$')


VALIDATIONS = {
    'byr': lambda v: YEAR.fullmatch(v) and 1920 <= int(v) <= 2002,
    'iyr': lambda v: YEAR.fullmatch(v) and 2010 <= int(v) <= 2020,
    'eyr': lambda v: YEAR.fullmatch(v) and 2020 <= int(v) <= 2030,
    'hgt': lambda v: HEIGHT.fullmatch(v) and (
        (v[-2:] == 'cm' and 150 <= int(v[:-2]) <= 193) or
        (v[-2:] == 'in' and 59 <= int(v[:-2]) <= 76)),
    'hcl': lambda v: HEX_COLOR.fullmatch(v),
    'ecl': lambda v: v in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'],
    'pid': lambda v: PID.fullmatch(v),
}


def read_input():
    passports = []
    passport = {}
    for line in open('input.txt', 'r').readlines():
        line = line.strip()
        if len(line) == 0:
            passports.append(passport)
            passport = {}
        else:
            passport.update({f[0]: f[1] for f in [
                field.split(':', 2) for field in line.split(' ')
            ]})
    if passport != {}:
        passports.append(passport)
    return passports


def is_valid(passport):
    for field, validation in VALIDATIONS.items():
        if field not in passport:
            print('Passport {0} is missing {1}.'.format(passport, field))
            return False
        if not validation(passport[field]):
            print('Passport {0} field {1} failed validation.'.format(passport, field))
            return False
    return True


def count_valid_passports(passports):
    count = 0
    for passport in passports:
        count += 1 if is_valid(passport) else 0
    return count


if __name__ == '__main__':
    print('{0} valid passports.'.format(count_valid_passports(read_input())))
