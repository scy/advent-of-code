REQUIRED_FIELDS = [
    'byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid',
]


def read_input():
    passports = []
    passport = {}
    for line in open('input.txt', 'r').readlines():
        line = line.strip()
        if len(line) == 0:
            passports.append(passport)
            passport = {}
        else:
            fields = line.split(' ')
            for field in fields:
                k, v = field.split(':', 2)
                passport[k] = v
    if passport != {}:
        passports.append(passport)
    return passports


def is_valid(passport):
    for field in REQUIRED_FIELDS:
        if field not in passport:
            return False
    return True


def count_valid_passports(passports):
    count = 0
    for passport in passports:
        count += 1 if is_valid(passport) else 0
    return count


if __name__ == '__main__':
    print('{0} valid passports.'.format(count_valid_passports(read_input())))
