#   nw ne        (-1,-1) ( 0,-1)
#  w  x  e   (-1, 0) ( 0, 0) ( 1, 0)
#   sw se        (-1, 1) ( 0, 1)
#            (-1, 2) ( 0, 2) ( 1, 2)


class Floor:
    MOVEMENTS = ['ne', 'e', 'se', 'sw', 'w', 'nw']

    def __init__(self):
        self.tiles = {}  # (x,y) => bool (False: white, True: black)

    def get_pos(self, path, pos=(0, 0)):
        for move in self.split_path(path):
            even = pos[1] % 2 == 0
            if move == 'e':
                pos = (pos[0]+1, pos[1])
            elif move == 'w':
                pos = (pos[0]-1, pos[1])
            elif move == 'ne':
                pos = (pos[0], pos[1]-1) if even else (pos[0]+1, pos[1]-1)
            elif move == 'se':
                pos = (pos[0], pos[1]+1) if even else (pos[0]+1, pos[1]+1)
            elif move == 'nw':
                pos = (pos[0]-1, pos[1]-1) if even else (pos[0], pos[1]-1)
            elif move == 'sw':
                pos = (pos[0]-1, pos[1]+1) if even else (pos[0], pos[1]+1)
        return pos

    def get_tile(self, path, pos=(0, 0)):
        return self.tiles.get(self.get_pos(path, pos), False)

    def flip_tile(self, path, pos=(0, 0)):
        pos = self.get_pos(path, pos)
        self.tiles[pos] = new_value = not self.tiles.get(pos, False)
        if new_value:  # flipped to black
            for adj in [self.get_pos(path, pos) for path in self.MOVEMENTS]:
                if adj not in self.tiles:
                    self.tiles[adj] = False

    def count_adjacent_black_tiles(self, pos):
        adjacent = [self.get_tile(path, pos) for path in self.MOVEMENTS]
        return sum(adjacent)  # because True is 1 and False is 0

    def split_path(self, path):
        buf = ''
        split = []
        for char in path:
            if char == 's' or char == 'n':
                buf = char
            elif char == 'e' or char == 'w':
                split.append(buf + char)
                buf = ''
            else:
                raise ValueError('invalid path char: {0}'.format(char))
        return split

    def read_input(self):
        with open('input.txt', 'r') as f:
            while True:
                line = f.readline()
                if len(line) == 0:
                    break
                self.flip_tile(line.strip())

    def count_black_tiles(self):
        return len([val for val in self.tiles.values() if val])

    def iterate(self, count=1):
        for _ in range(count):
            to_flip = []
            for pos, black in self.tiles.items():
                abt = self.count_adjacent_black_tiles(pos)
                if black and (abt == 0 or abt > 2):
                    to_flip.append(pos)
                elif not black and abt == 2:
                    to_flip.append(pos)
            for pos in to_flip:
                self.flip_tile([], pos)


if __name__ == '__main__':
    f = Floor()
    f.read_input()
    f.iterate(100)
    print('There are now {0} tiles with the black side up.'.format(
        f.count_black_tiles()))
