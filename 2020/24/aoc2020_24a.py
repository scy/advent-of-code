#   nw ne        (-1,-1) ( 0,-1)
#  w  x  e   (-1, 0) ( 0, 0) ( 1, 0)
#   sw se        (-1, 1) ( 0, 1)
#            (-1, 2) ( 0, 2) ( 1, 2)


class Floor:

    def __init__(self):
        self.tiles = {}  # (x,y) => bool (False: white, True: black)

    def flip_tile(self, path):
        pos = (0, 0)
        for move in self.split_path(path):
            even = pos[1] % 2 == 0
            if move == 'e':
                pos = (pos[0]+1, pos[1])
            elif move == 'w':
                pos = (pos[0]-1, pos[1])
            elif move == 'ne':
                pos = (pos[0], pos[1]-1) if even else (pos[0]+1, pos[1]-1)
            elif move == 'se':
                pos = (pos[0], pos[1]+1) if even else (pos[0]+1, pos[1]+1)
            elif move == 'nw':
                pos = (pos[0]-1, pos[1]-1) if even else (pos[0], pos[1]-1)
            elif move == 'sw':
                pos = (pos[0]-1, pos[1]+1) if even else (pos[0], pos[1]+1)
        self.tiles[pos] = not self.tiles.get(pos, False)

    def split_path(self, path):
        buf = ''
        split = []
        for char in path:
            if char == 's' or char == 'n':
                buf = char
            elif char == 'e' or char == 'w':
                split.append(buf + char)
                buf = ''
            else:
                raise ValueError('invalid path char: {0}'.format(char))
        return split

    def read_input(self):
        with open('input.txt', 'r') as f:
            while True:
                line = f.readline()
                if len(line) == 0:
                    break
                self.flip_tile(line.strip())

    def count_black_tiles(self):
        return len([val for val in self.tiles.values() if val])


if __name__ == '__main__':
    f = Floor()
    f.read_input()
    print('There are now {0} tiles with the black side up.'.format(
        f.count_black_tiles()))
