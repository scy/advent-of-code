def read_input():
    input = []
    for line in open('input.txt', 'r').readlines():
        count, letter, password = line.strip().split(' ')
        a, b = count.split('-')
        letter = letter[0]
        input.append((int(a), int(b), letter, password))
    return input


def check_policy(a, b, letter, password):
    return (password[a-1] == letter) ^ (password[b-1] == letter)


def count_valid_passwords():
    valid = 0
    for pwtuple in read_input():
        valid += int(check_policy(*pwtuple))
    return valid


if __name__ == '__main__':
    print('Found {0} valid passwords.'.format(count_valid_passwords()))
