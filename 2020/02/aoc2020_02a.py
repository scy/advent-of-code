def read_input():
    input = []
    for line in open('input.txt', 'r').readlines():
        count, letter, password = line.strip().split(' ')
        min, max = count.split('-')
        letter = letter[0]
        input.append((int(min), int(max), letter, password))
    return input


def check_policy(min, max, letter, password):
    count = password.count(letter)
    return count >= min and count <= max


def count_valid_passwords():
    valid = 0
    for pwtuple in read_input():
        valid += int(check_policy(*pwtuple))
    return valid


if __name__ == '__main__':
    print('Found {0} valid passwords.'.format(count_valid_passwords()))
