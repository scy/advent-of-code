### WARNING: I've given up on this puzzle. The following code is incomplete.

import math


def str_reverse(s):
    ''.join(reversed(s))


class Tile:

    def __init__(self, lines):
        if not len(lines) == 11:
            raise ValueError('Input has to be 11 lines but is {0}'.format(
                len(lines)))
        if not lines[0].startswith('Tile '):
            raise ValueError('Input has to start with "Tile ####:"')
        self.id = int(lines[0].strip().split(' ')[1][:-1])
        self.data = [line.strip() for line in lines[1:]]
        self.rotate = 0
        self.flipped = False
        self.candidates = []
        edges = [
            self.data[0],
            self.data[-1],
            ''.join([line[0] for line in self.data]),
            ''.join([line[-1] for line in self.data]),
        ]
        self.edges = []
        for edge in edges:
            self.edges.append(edge)
            self.edges.append(str_reverse(edge))

    def edge(self, which, flip):
        if which == 0:
            edge = self.data[0]
        elif which == 90:
            edge = ''.join([line[-1] for line in self.data])
        elif which == 180:
            edge = self.data[-1]
        elif which == 270:
            edge = ''.join([line[0] for line in self.data])
        else:
            raise ValueError('invalid value "{0}" for which'.format(which))
        return str_reverse(edge) if flip else edge

    def fits_tile(self, tile):
        if tile is self:
            return False
        for edge in self.edges:
            if edge in tile.edges:
                return True
        return False

    def tile_edge_fit_ways(self, tile, edge):
        if tile is self:
            return []
        edge = tile.edge(edge)
        fits = []
        if edge == self.data[0]:
            fits.append((0, False))
        elif edge == str_reverse(self.data[0]):
            fits.append((0, True))
        elif edge == self.data[-1]:
            fits.append((180, True))
        elif edge == str_reverse(self.data[-1]):
            fits.append((180, False))
        elif edge ==


class Stitch:

    def __init__(self):
        self.tiles = []
        self.edge_len = None

    def read_input(self):
        tile_lines = []
        with open('input.txt', 'r') as f:
            while True:
                line = f.readline()
                if len(line) == 0:
                    if len(tile_lines):
                        self.tiles.append(Tile(tile_lines))
                        tile_lines = []
                    break
                line = line.strip()
                if len(line) == 0:
                    self.tiles.append(Tile(tile_lines))
                    tile_lines = []
                else:
                    tile_lines.append(line)
        self.edge_len = int(math.sqrt(len(self.tiles)))

    def find_candidates(self):
        for a in self.tiles:
            a.candidates = [b for b in self.tiles if a.fits_tile(b)]

    def place_tiles(self, field={}, prev_tile=None, pos=(0,0)):
        candidates = prev_tile.candidates if prev_tile is not None else self.tiles
        for candidate in candidates:



if __name__ == '__main__':
    s = Stitch()
    s.read_input()
    s.find_candidates()
