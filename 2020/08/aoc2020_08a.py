class Instruction:

    def __init__(self, line):
        self.cmd, self.arg = line.strip().split(' ')
        self.arg = int(self.arg)
        self.run_count = 0


class GameConsole:

    def __init__(self):
        self.accumulator = 0
        self.instr_idx = 0
        self.instructions = []
        with open('input.txt', 'r') as f:
            while True:
                line = f.readline()
                if not len(line):
                    break
                self.instructions.append(Instruction(line))

    def run(self):
        while True:
            instr = self.instructions[self.instr_idx]

            if instr.run_count != 0:
                raise RuntimeError('second execution, acc is {0}'.format(
                    self.accumulator))
            instr.run_count += 1

            if instr.cmd == 'nop':
                pass
            elif instr.cmd == 'acc':
                self.accumulator += instr.arg
            elif instr.cmd == 'jmp':
                self.instr_idx += instr.arg
            else:
                raise ValueError('unknown command "{0}"'.format(instr.cmd))

            if instr.cmd != 'jmp':
                self.instr_idx += 1

            if self.instr_idx >= len(self.instructions) \
                    or self.instr_idx < 0:
                raise ValueError('instruction index out of range')


if __name__ == '__main__':
    gc = GameConsole()
    gc.run()
