class Memory:

    def __init__(self):
        self.turn = 0
        self.seen = {}
        self.last_num = None

    def next(self):
        with open('input.txt', 'r') as f:
            for num in f.readline().strip().split(','):
                self.turn += 1
                yield self.say(int(num))
        while True:
            self.turn += 1
            if len(self.seen[self.last_num]) == 1:
                yield self.say(0)
            else:
                numseen = self.seen[self.last_num]
                yield self.say(numseen[-1] - numseen[-2])


    def say(self, num):
        self.last_num = num
        self.seen.setdefault(num, []).append(self.turn)
        return num


if __name__ == '__main__':
    m = Memory()
    gen = m.next()
    until = 2020
    while m.turn < until:
        num = next(gen)
    print('The {0}th number is {1}.'.format(
        until, num))
