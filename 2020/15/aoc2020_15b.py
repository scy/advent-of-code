class Memory:

    def __init__(self):
        self.turn = 0
        self.prev_seen = {}
        self.last_num = None

    def next(self):
        with open('input.txt', 'r') as f:
            for num in f.readline().strip().split(','):
                self.turn += 1
                yield self.say(int(num))
        while True:
            self.turn += 1
            if self.last_num not in self.prev_seen:
                yield self.say(0)
            else:
                yield self.say(self.turn-1 - self.prev_seen[self.last_num])


    def say(self, num):
        self.prev_seen[self.last_num] = self.turn - 1
        self.last_num = num
        return num


if __name__ == '__main__':
    m = Memory()
    gen = m.next()
    until = 30_000_000
    step = until // 100
    while m.turn < until:
        num = next(gen)
        if m.turn % step == 0:
            print('{0} %'.format(round(m.turn / until * 100)))
    print('The {0}th number is {1}.'.format(
        until, num))
