import re


class Rule:
    RULE_RE = re.compile('^(.+) bags contain (.+)\\.$')
    CONTAIN_RE = re.compile('^(no other bags|([0-9]+) (.+) bags?)$')

    def __init__(self, rule):
        match = self.RULE_RE.fullmatch(rule)
        if not match:
            raise RuntimeError('rule "{0}" seems invalid'.format(rule))
        self.color = match[1]
        self.contains = []
        for contains in match[2].split(', '):
            submatch = self.CONTAIN_RE.fullmatch(contains)
            if not submatch:
                raise RuntimeError(
                    'rule "{0}" contains strange submatch "{1}"'.format(
                        rule, match[2]))
            if submatch[1] != 'no other bags':
                self.contains.append((int(submatch[2]), submatch[3]))

    def __str__(self):
        return '{0} <{1}>'.format(
            self.color,
            self.contains,
        )

    def can_contain(self, find_color, all_rules):
        for count, color in self.contains:
            if all_rules[color].can_contain(find_color, all_rules):
                return True
        return find_color in [tup[1] for tup in self.contains]


def read_input():
    with open('input.txt', 'r') as f:
        while True:
            line = f.readline()
            if not len(line):
                return
            yield Rule(line.strip())


def collect_rules(rules):
    rules = {rule.color: rule for rule in rules}
    return rules


def find_iterative(find_color, rules):
    return [rule
            for color, rule in rules.items()
            if rule.can_contain(find_color, rules)]


if __name__ == '__main__':
    find_color = 'shiny gold'
    print('{0} colors can contain {1} directly or indirectly.'.format(
        len(find_iterative(find_color, collect_rules(read_input()))),
        find_color,
    ))
