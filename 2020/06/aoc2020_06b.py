def read_input():
    with open('input.txt', 'r') as f:
        while True:
            line = f.readline()
            if line == '':
                return
            yield line.strip()


def group_answers(input):
    answers = {}
    persons = 0
    for line in input:
        if len(line) == 0:
            yield ''.join([letter
                           for letter, count in answers.items()
                           if count == persons])
            answers = {}
            persons = 0
        else:
            persons += 1
            for letter in line:
                if letter not in answers:
                    answers[letter] = 0
                answers[letter] += 1
    yield ''.join([letter
                   for letter, count in answers.items()
                   if count == persons])


def count_answers(answers):
    count = 0
    for answer in answers:
        count += len(answer)
    return count


if __name__ == '__main__':
    print("The sum of each group's answer count is {0}".format(
        count_answers(group_answers(read_input()))))
