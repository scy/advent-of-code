def read_input():
    with open('input.txt', 'r') as f:
        while True:
            line = f.readline()
            if line == '':
                return
            yield line.strip()


def group_answers(input):
    answers = set()
    for line in input:
        if len(line) == 0:
            yield ''.join(answers)
            answers = set()
        else:
            for letter in line:
                answers.add(letter)
    yield ''.join(answers)


def count_answers(answers):
    count = 0
    for answer in answers:
        count += len(answer)
    return count


if __name__ == '__main__':
    print("The sum of each group's answer count is {0}".format(
        count_answers(group_answers(read_input()))))
