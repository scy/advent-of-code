class PocketDimension:

    def __init__(self):
        self.cubes = {}
        # Lowest and highest coordinate for each dimension:
        self.boundaries = 3 * [(0, 0)]

    def __str__(self):
        result = 'Boundaries: {0}\n'.format(self.boundaries)
        for z in self.range_for_dimension(2):
            result += 'z={0}\n'.format(z)
            for y in self.range_for_dimension(1):
                for x in self.range_for_dimension(0):
                    result += '#' if self.get(x, y, z) else '.'
                result += '\n'
            result += '\n'
        return result

    def range_for_dimension(self, dim, expand=0):
        return range(
            self.boundaries[dim][0] - expand,
            self.boundaries[dim][1] + expand + 1)

    def coordinates_iter(self, expand=0):
        for z in self.range_for_dimension(2, expand):
            for y in self.range_for_dimension(1, expand):
                for x in self.range_for_dimension(0, expand):
                    yield (x, y, z)

    def read_input(self, lines):
        for y, line in enumerate(lines):
            for x, char in enumerate(line.strip()):
                self.set(x, y, 0, char == '#')

    def set(self, x, y, z, active):
        yz = self.cubes.setdefault(x, {})
        z_dict = yz.setdefault(y, {})
        z_dict[z] = active
        self.boundaries = [
            (min(self.boundaries[0][0], x), max(self.boundaries[0][1], x)),
            (min(self.boundaries[1][0], y), max(self.boundaries[1][1], y)),
            (min(self.boundaries[2][0], z), max(self.boundaries[2][1], z)),
        ]

    def get(self, x, y, z):
        return self.cubes.get(x, {}).get(y, {}).get(z, False)

    def count_active_neighbors(self, x, y, z):
        count = 0
        for ix in range(x-1, x+2):
            for iy in range(y-1, y+2):
                for iz in range(z-1, z+2):
                    if ix == x and iy == y and iz == z:
                        continue
                    count += 1 if self.get(ix, iy, iz) else 0
        return count

    def cycle(self):
        new = PocketDimension()
        for x, y, z in self.coordinates_iter(1):
            neighbors = self.count_active_neighbors(x, y, z)
            active = self.get(x, y, z)
            if active and (neighbors == 2 or neighbors == 3):
                new.set(x, y, z, True)
            elif not active and neighbors == 3:
                new.set(x, y, z, True)
        self.cubes = new.cubes
        self.boundaries = new.boundaries

    def cycles(self, count=1):
        for _ in range(count):
            self.cycle()

    def count_active(self):
        return sum([
            1 if self.get(x, y, z) else 0
            for x, y, z in self.coordinates_iter()
        ])


if __name__ == '__main__':
    num_cycles = 6
    pd = PocketDimension()
    with open('input.txt', 'r') as f:
        pd.read_input(f.readlines())
    pd.cycles(num_cycles)
    print('After {0} cycles, {1} cubes are active.'.format(
        num_cycles, pd.count_active()))
