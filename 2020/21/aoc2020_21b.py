import re


class Food:
    FOOD_RE = re.compile('^(.+) \(contains (.+)\)$')

    def __init__(self, line):
        match = self.FOOD_RE.match(line.strip())
        if not match:
            raise ValueError('strange line: ' + line.strip())
        self.ingredients = match[1].split(' ')
        self.allergens = match[2].split(', ')

    def __str__(self):
        return '{0} ({1})'.format(self.ingredients, self.allergens)

    def update_ingredient_allergens(self, allergens):
        pass


class Foods:

    def __init__(self):
        self.foods = []
        self.ingredients = {}
        self.allergens = {}
        self.ingredient_allergens = {}

    def read_input(self):
        with open('input.txt', 'r') as f:
            lines = f.readlines()
            for line in lines:
                food = Food(line)
                for ingr in food.ingredients:
                    self.ingredients.setdefault(ingr, []).append(food)
                for allerg in food.allergens:
                    self.allergens.setdefault(allerg, []).append(food)
                self.foods.append(food)

    def compute_allergen_candidates(self):
        for ingr, foods in self.ingredients.items():
            possible_allergens = set()
            for food in foods:
                for allergen in food.allergens:
                    possible_allergens.add(allergen)
            for allergen in list(possible_allergens):
                for food in self.allergens[allergen]:
                    if ingr not in food.ingredients:
                        possible_allergens.remove(allergen)
                        break
            self.ingredient_allergens[ingr] = possible_allergens

    def count_no_allergen_ingredient_appearances(self):
        count = 0
        for ingr, possible_allergens in self.ingredient_allergens.items():
            if len(possible_allergens) > 0:
                continue
            count += len(self.ingredients[ingr])
        return count

    def compute_allergens(self):
        ingr_to_allergen = []
        while True:
            ingredients = [
                (ingr, pa) for (ingr, pa) in self.ingredient_allergens.items()
                if len(pa) == 1]
            if len(ingredients) == 0:
                break
            allergen = ingredients[0][1].pop()
            for ingr, pa in self.ingredient_allergens.items():
                pa.discard(allergen)
            ingr_to_allergen.append((ingredients[0][0], allergen))
        return ingr_to_allergen


if __name__ == '__main__':
    f = Foods()
    f.read_input()
    f.compute_allergen_candidates()
    ingr_to_allergen = f.compute_allergens()
    ingr_to_allergen.sort(key=lambda tup: tup[1])
    print('Canonical dangerous ingredient list:\n{0}'.format(
        ','.join([tup[0] for tup in ingr_to_allergen])))
