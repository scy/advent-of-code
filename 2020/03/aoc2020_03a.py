def read_input():
    rows = []
    width = None
    for line in open('input.txt', 'r').readlines():
        line = line.strip()
        if width is None:
            width = len(line)
        rows.append(int(line.replace('.', '0').replace('#', '1'), 2))
    return width, rows


def count_trees(width, input):
    x = y = trees = 0
    while y+1 < len(input):
        x = (x + 3) % width
        y += 1
        mask = 1 << (width - x - 1)
        trees += 1 if input[y] & mask else 0
    return trees


if __name__ == '__main__':
    print('{0} trees encountered.'.format(count_trees(*read_input())))
