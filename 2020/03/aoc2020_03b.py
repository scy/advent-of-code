TEST_SLOPES = (
    (1, 1),
    (3, 1),
    (5, 1),
    (7, 1),
    (1, 2),
)


def read_input():
    rows = []
    width = None
    for line in open('input.txt', 'r').readlines():
        line = line.strip()
        if width is None:
            width = len(line)
        rows.append(int(line.replace('.', '0').replace('#', '1'), 2))
    return width, rows


def count_trees(width, input, right=3, down=1):
    x = y = trees = 0
    while y+1 < len(input):
        x = (x + right) % width
        y += down
        mask = 1 << (width - x - 1)
        trees += 1 if input[y] & mask else 0
    return trees


def multiply_slopes(width, input, test_slopes):
    trees = 1
    for slope in test_slopes:
        trees *= count_trees(width, input, *slope)
    return trees


if __name__ == '__main__':
    print('Multiplication results in {0} trees.'.format(
        multiply_slopes(*read_input(), TEST_SLOPES)))
