# Heavily based upon <https://rosettacode.org/wiki/Chinese_remainder_theorem#Python>.
# In fact, I have pretty much no idea what I'm doing. <dog.jpg>


from functools import reduce


def read_input():
    with open('input.txt', 'r') as f:
        lines = f.readlines()
    return [int(bus) if bus != 'x' else bus for bus in lines[1].strip().split(',')]


def massage_input(input):
    n = []
    a = []
    for off, bus in enumerate(input):
        if bus != 'x':
            n.append(bus)
            a.append(-off)
    return n, a


def chinese_remainder(n, a):
    sum = 0
    prod = reduce(lambda a, b: a*b, n)
    for n_i, a_i in zip(n, a):
        p = prod // n_i
        sum += a_i * mul_inv(p, n_i) * p
    return sum % prod


def mul_inv(a, b):
    b0 = b
    x0, x1 = 0, 1
    if b == 1:
        return 1
    while a > 1:
        q = a // b
        a, b = b, a%b
        x0, x1 = x1 - q * x0, x0
    if x1 < 0:
        x1 += b0
    return x1


if __name__ == '__main__':
    print(chinese_remainder(*massage_input(read_input())))
