def read_input():
    with open('input.txt', 'r') as f:
        lines = f.readlines()
    buses = [int(bus) for bus in lines[1].strip().split(',') if bus != 'x']
    return int(lines[0].strip()), buses


def find_earliest_departure(start, buses):
    return min(
        [(bus, 0 if start % bus == 0 else bus - (start % bus)) for bus in buses],
        key=lambda tup: tup[1]
    )


if __name__ == '__main__':
    start, buses = read_input()
    solution = find_earliest_departure(start, buses)
    print('You ride the {0} bus in {1} minutes (at {2}) (result is {3}).'.format(
        solution[0], solution[1], start + solution[1], solution[0] * solution[1]))
