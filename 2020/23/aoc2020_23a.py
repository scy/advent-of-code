class CrabCups:

    def __init__(self, cups):
        self.cups = [int(cup) for cup in cups]
        self.current = 0
        self.lowest = min(self.cups)
        self.highest = max(self.cups)
        self.move_no = 0

    def format_cups(self):
        return 'cups: {0}'.format(''.join([
            '({0})'.format(cup) if idx == self.current else ' {0} '.format(cup)
            for (idx, cup) in enumerate(self.cups)
            ]))

    def move(self):
        self.move_no += 1
        print('-- move {0} --'.format(self.move_no))
        print(self.format_cups())

        taken = self.take(3)
        print('pick up: {0}'.format(', '.join([str(item) for item in taken])))

        dest_idx = self.destination()
        print('destination: {0}'.format(self.cups[dest_idx]))

        self.insert(dest_idx, taken)
        self.current = (self.current + 1) % len(self.cups)
        print('')

    def moves(self, count):
        for _ in range(count):
            self.move()
        print('-- final --\n' + self.format_cups())

    def take(self, count):
        taken = []
        pos = self.current
        posval = self.cups[self.current]
        for _ in range(count):
            pos = (pos + 1) % len(self.cups)
            taken.append(self.cups[pos])
        self.cups = [cup for cup in self.cups if cup not in taken]
        self.current = self.cups.index(posval)
        return taken

    def destination(self):
        dest = self.cups[self.current] - 1
        while True:
            if dest < self.lowest:
                dest = self.highest
            try:
                return self.cups.index(dest)
            except ValueError:
                dest -= 1

    def insert(self, after, items):
        posval = self.cups[self.current]
        self.cups = self.cups[:after+1] + items + self.cups[after+1:]
        self.current = self.cups.index(posval)

    def answer(self):
        answer = ''
        pos = (self.cups.index(1) + 1) % len(self.cups)
        while self.cups[pos] != 1:
            answer += str(self.cups[pos])
            pos = (pos + 1) % len(self.cups)
        return answer


if __name__ == '__main__':
    # cc = CrabCups('389125467')  # Example.
    cc = CrabCups('137826495')
    cc.moves(100)
    print('The answer is: {0}.'.format(cc.answer()))
