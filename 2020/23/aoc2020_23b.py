class Cup:

    def __init__(self, label):
        self.label = label
        self.next = None

class CrabCups:

    def __init__(self, labels, cup_count=1_000_000):
        self.move_no = 0
        self.cups = {}
        self.current = None
        self.last_cup = None
        max_label = None
        min_label = None

        # Add labels from puzzle input.
        for label in labels:
            label = int(label)
            self.add_cup(label)
            # Keep pointer to first cup.
            if self.current is None:
                self.current = self.last_cup
            if min_label is None or min_label > label:
                min_label = label
            if max_label is None or max_label < label:
                max_label = label

        # Fill up until cup_count.
        for num in range(max_label+1, cup_count+1):
            self.add_cup(num)
        self.lowest = min_label
        self.highest = cup_count

        # Connect last item to first.
        self.last_cup.next = self.current

    def add_cup(self, label):
        self.cups[label] = cup = Cup(label)
        if self.last_cup is not None:
            self.last_cup.next = cup
        self.last_cup = cup

    def move(self):
        self.move_no += 1
        if self.move_no % 10_000 == 0:
            print(self.move_no)

        taken = self.take(3)

        dest = self.destination(taken)

        self.splice(dest, taken)
        self.current = self.current.next

    def moves(self, count):
        for _ in range(count):
            self.move()

    def take(self, count):
        taken = []
        current = self.current
        for _ in range(count):
            current = current.next
            taken.append(current)
        return taken

    def destination(self, taken):
        dest = self.current
        while True:
            new_label = dest.label - 1
            if new_label not in self.cups:
                new_label = self.highest
            dest = self.cups[new_label]
            if dest not in taken:
                return dest

    def splice(self, after, items):
        after_after = after.next
        after.next = items[0]
        self.current.next = items[-1].next
        items[-1].next = after_after

    def answer(self):
        answer = []
        curr = self.cups[1]
        for _ in range(2):
            curr = curr.next
            answer.append(curr.label)
        return answer[0] * answer[1]


if __name__ == '__main__':
    # cc = CrabCups('389125467')  # Example.
    cc = CrabCups('137826495')
    cc.moves(10_000_000)
    print('The answer is: {0}.'.format(cc.answer()))
