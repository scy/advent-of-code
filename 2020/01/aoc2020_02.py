def read_input():
    return [int(x) for x in open('input.txt', 'r').readlines()]


def find_elements(input, sum=2020):
    for a_idx, a_val in enumerate(input):
        for b_idx, b_val in enumerate(input[a_idx+1:]):
            for c_val in input[b_idx+1:]:
                if a_val + b_val + c_val == sum:
                    return (a_val, b_val, c_val)


if __name__ == '__main__':
    a, b, c = find_elements(read_input())
    print('{0} * {1} * {2} = {3}'.format(a, b, c, a * b * c))
