def read_input():
    return [int(x) for x in open('input.txt', 'r').readlines()]


def find_elements(input, sum=2020):
    for a in input:
        for b in input:
            if a + b == sum:
                return (a, b)


if __name__ == '__main__':
    a, b = find_elements(read_input())
    print('{0} * {1} = {2}'.format(a, b, a * b))
