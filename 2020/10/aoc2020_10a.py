def read_input():
    with open('input.txt', 'r') as f:
        while True:
            line = f.readline()
            if not len(line):
                return
            yield int(line.strip())


def get_adapter_list(input):
    adapters = list(input)
    adapters.sort()
    return adapters


def chain_adapters(adapters):
    previous = 0
    diff_counter = dict.fromkeys(range(1, 4), 0)
    for adapter in adapters:
        diff = adapter - previous
        if diff <= 0 or diff > 3:
            raise ValueError('difference between {0} and {1} is {2}'.format(
                previous, adapter, diff))
        diff_counter[diff] += 1
        previous = adapter
    diff_counter[3] += 1
    return diff_counter


if __name__ == '__main__':
    chain = chain_adapters(get_adapter_list(read_input()))
    print('The differences are {0}, product of 1s and 3s is {1}.'.format(
        chain, chain[1] * chain[3]))
