def read_input():
    with open('input.txt', 'r') as f:
        while True:
            line = f.readline()
            if not len(line):
                return
            yield int(line.strip())


def adapter_list(input):
    adapters = list(input)
    adapters.sort()
    return adapters


def chain_adapters(adapters):
    previous = 0
    diff_counter = dict.fromkeys(range(1, 4), 0)
    for adapter in adapters:
        diff = adapter - previous
        if diff <= 0 or diff > 3:
            raise ValueError('difference between {0} and {1} is {2}'.format(
                previous, adapter, diff))
        diff_counter[diff] += 1
        previous = adapter
    diff_counter[3] += 1
    return diff_counter


def diff_list(adapters):
    adapters = [0] + adapters + [adapters[-1] + 3]
    diff_list = []
    for i in range(1, len(adapters)):
        diff_list.append(adapters[i] - adapters[i-1])
    return diff_list


def consecutive_ones(diff_list):
    result = []
    ones = 0
    for diff in diff_list:
        if diff != 1 and ones:
            result.append(ones)
            ones = 0
        if diff == 1:
            ones += 1
    if ones:
        result.append(ones)
    return result


def possibilities(ones):
    result = 1
    for one_length in ones:
        if one_length == 1:
            pass
        elif one_length == 2:
            result *= 2
        elif one_length == 3:
            result *= 4
        elif one_length == 4:
            result *= 7
        else:
            raise NotImplementedError(
                'possibilities for {0} consecutive ones unknown'.format(
                    one_length))
    return result


if __name__ == '__main__':
    print(possibilities(consecutive_ones(diff_list(adapter_list(read_input())))))
