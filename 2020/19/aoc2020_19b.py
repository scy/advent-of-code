import regex


class Matcher:

    def __init__(self):
        self.rule = None
        self.messages = []
        self.read_input()

    def read_input(self):
        reading_rules = True
        rules = {}
        with open('input.txt', 'r') as f:
            while True:
                line = f.readline()
                if len(line) == 0:
                    break
                line = line.strip()
                if len(line) == 0:
                    if reading_rules:
                        reading_rules = False
                    else:
                        raise ValueError('additional empty line')
                elif reading_rules:
                    num, rule = line.split(':', 2)
                    rules[int(num)] = (False, rule[1:])
                else:
                    self.messages.append(line)
        # Monkey-patch rules 8 and 11 according to the new rules.
        self.resolve_rule(rules, 8)
        rules[8] = (True, '({0})+'.format(rules[8][1]))
        r31 = '(' + self.resolve_rule(rules, 31) + ')'
        r42 = '(' + self.resolve_rule(rules, 42) + ')'
        rules[11] = (True, '(?P<rec>{0}(?&rec)?{1})'.format(r42, r31))
        self.rule = regex.compile(self.resolve_rule(rules, 0))

    def resolve_rule(self, rules, num):
        resolved, rule = rules[num]
        if resolved:
            return rule
        components = rule.split(' ')
        result = []
        for component in components:
            if component == '|':
                result.append('|')
            elif component[0] == '"':
                result.append(component[1])
            else:
                resolved = self.resolve_rule(rules, int(component))
                result.append(resolved if len(resolved) == 1 else ('(' + resolved + ')'))
        rules[num] = (True, ''.join(result))
        return rules[num][1]

    def count_valid_messages(self):
        return len([msg for msg in self.messages if self.rule.fullmatch(msg)])


if __name__ == '__main__':
    m = Matcher()
    print('{0} of {1} messages are valid.'.format(
        m.count_valid_messages(), len(m.messages)))
