class XMAS:
    CONTEXT_LEN = 25

    def __init__(self, stream):
        self.stream = stream
        self.context = []
        for _ in range(self.CONTEXT_LEN):
            self.read_next(False)

    def read_next(self, validate=True):
        next_number = next(self.stream)
        if validate and not self.is_valid_next(next_number):
            raise ValueError('number {0} is invalid for context {1}'.format(
                next_number, self.context))
        self.context.append(next_number)
        if len(self.context) > self.CONTEXT_LEN:
            self.context = self.context[-self.CONTEXT_LEN:]

    def read_all(self):
        while True:
            try:
                self.read_next()
            except StopIteration:
                return

    def is_valid_next(self, number):
        for a_idx, a in enumerate(self.context[:-1]):
            for b in self.context[a_idx+1:]:
                if a + b == number:
                    return True
        return False


def read_input():
    with open('input.txt', 'r') as f:
        while True:
            line = f.readline()
            if not len(line):
                return
            yield int(line.strip())


if __name__ == '__main__':
    x = XMAS(read_input())
    x.read_all()
