class XMAS:
    CONTEXT_LEN = 25

    def __init__(self, stream):
        self.stream = stream
        self.context = []
        self.previous = []
        self.invalid_number = None
        for _ in range(self.CONTEXT_LEN):
            self.read_next(False)

    def read_next(self, validate=True):
        next_number = next(self.stream)
        if validate and not self.is_valid_next(next_number):
            self.invalid_number = next_number
            raise ValueError('number {0} is invalid for context {1}'.format(
                next_number, self.context))
        self.context.append(next_number)
        self.previous.append(next_number)
        if len(self.context) > self.CONTEXT_LEN:
            self.context = self.context[-self.CONTEXT_LEN:]

    def read_all(self):
        while True:
            try:
                self.read_next()
            except StopIteration:
                return

    def is_valid_next(self, number):
        for a_idx, a in enumerate(self.context[:-1]):
            for b in self.context[a_idx+1:]:
                if a + b == number:
                    return True
        return False

    def find_slice(self, target_sum):
        for a in range(len(self.previous) - 1):
            for b in range(a + 1, len(self.previous) + 1):
                if sum(self.previous[a:b]) == target_sum:
                    return self.previous[a:b]
        return None


def read_input():
    with open('input.txt', 'r') as f:
        while True:
            line = f.readline()
            if not len(line):
                return
            yield int(line.strip())


if __name__ == '__main__':
    x = XMAS(read_input())
    try:
        x.read_all()
    except ValueError:
        pass
    slice = x.find_slice(x.invalid_number)
    print('Sum of smallest and largest number in slice {0} is {1}'.format(
        slice, min(slice) + max(slice)))
