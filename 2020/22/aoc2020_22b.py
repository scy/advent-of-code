class Combat:
    subgame_no = 1

    def __init__(self, game_no=None, decks=None):
        self.decks = {1: [], 2: []} if decks is None else decks
        self.round = 0
        self.no = 1 if game_no is None else game_no
        self.previous_configs = {}
        self.winner = None

    def configuration(self):
        return '{0};{1}'.format(self.list_deck(1), self.list_deck(2))

    def read_input(self):
        player = None
        with open('input.txt', 'r') as f:
            while True:
                line = f.readline()
                if len(line) == 0:
                    break
                line = line.strip()
                if line.startswith('Player '):
                    player = int(line[-2])
                    if player != 1 and player != 2:
                        raise ValueError('Invalid player number: {0}'.format(player))
                    continue
                if len(line) == 0:
                    continue
                self.decks[player].append(int(line))

    def list_deck(self, player):
        return ', '.join([str(card) for card in self.decks[player]])

    def print_deck(self, player):
        print("Player {0}'s deck: {1}".format(
            player,
            self.list_deck(player)))

    def play_round(self):
        self.round += 1
        print('-- Round {0} (Game {1}) --'.format(self.round, self.no))
        self.print_deck(1)
        self.print_deck(2)

        config = self.configuration()
        if config in self.previous_configs:
            # Player 1 wins as per the rules.
            return 1
        self.previous_configs[config] = True

        p1 = self.draw(1)
        print('Player 1 plays: {0}'.format(p1))
        p2 = self.draw(2)
        print('Player 2 plays: {0}'.format(p2))

        if len(self.decks[1]) >= p1 and len(self.decks[2]) >= p2:
            winner = self.play_subgame(p1, p2)
        else:
            winner = 1 if p1 > p2 else 2
        print('Player {0} wins round {1} of game {2}!\n'.format(
            winner, self.round, self.no))
        self.stash(winner,
            p1 if winner == 1 else p2,  # Winning card.
            p2 if winner == 1 else p1)  # Losing card.

        if len(self.decks[1]) == 0:
            return 2
        if len(self.decks[2]) == 0:
            return 1
        else:
            return None

    def play_game(self):
        print('=== Game {0} ===\n'.format(self.no))
        while True:
            winner = self.play_round()
            if winner is not None:
                break
        if self.no == 1:
            print('\n== Post-game results ==')
            self.print_deck(1)
            self.print_deck(2)
        self.winner = winner
        return winner

    def play_subgame(self, p1, p2):
        print('Playing a sub-game to determine the winner...\n')
        Combat.subgame_no += 1
        subgame = Combat(Combat.subgame_no, {
            1: self.decks[1][:p1],
            2: self.decks[2][:p2],
        })
        winner = subgame.play_game()
        print('The winner of game {0} is player {1}!'.format(
            Combat.subgame_no, winner))
        print('\n...anyway, back to game {0}.'.format(self.no))
        return winner

    def draw(self, player):
        return self.decks[player].pop(0)

    def stash(self, player, winning_card, losing_card):
        self.decks[player].extend([winning_card, losing_card])

    def winner_score(self):
        if self.winner is None:
            raise RuntimeError('no winner yet')
        l = len(self.decks[self.winner])
        return sum([(l-pos)*card for (pos, card) in enumerate(self.decks[self.winner])])


if __name__ == '__main__':
    c = Combat()
    c.read_input()
    c.play_game()
    print('Winning score is {0}.'.format(c.winner_score()))
