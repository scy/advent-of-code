class Combat:

    def __init__(self):
        self.decks = {1: [], 2: []}
        self.round = 0

    def read_input(self):
        player = None
        with open('input.txt', 'r') as f:
            while True:
                line = f.readline()
                if len(line) == 0:
                    break
                line = line.strip()
                if line.startswith('Player '):
                    player = int(line[-2])
                    if player != 1 and player != 2:
                        raise ValueError('Invalid player number: {0}'.format(player))
                    continue
                if len(line) == 0:
                    continue
                self.decks[player].append(int(line))

    def print_deck(self, player):
        print("Player {0}'s deck: {1}".format(
            player,
            ', '.join([str(card) for card in self.decks[player]])))

    def play_round(self):
        self.round += 1
        print('-- Round {0} --'.format(self.round))
        self.print_deck(1)
        self.print_deck(2)
        p1 = self.draw(1)
        print('Player 1 plays: {0}'.format(p1))
        p2 = self.draw(2)
        print('Player 2 plays: {0}'.format(p2))
        winner = 1 if p1 > p2 else 2
        print('Player {0} wins the round!\n'.format(winner))
        self.stash(winner, [p1, p2])

    def play_game(self):
        while self.winner() is None:
            self.play_round()
        print('\n== Post-game results ==')
        self.print_deck(1)
        self.print_deck(2)

    def draw(self, player):
        return self.decks[player].pop(0)

    def stash(self, player, cards):
        cards.sort(reverse=True)
        self.decks[player].extend(cards)

    def winner(self):
        if len(self.decks[1]) == 0:
            return 2
        if len(self.decks[2]) == 0:
            return 1
        return None

    def winner_score(self):
        winner = self.winner()
        if winner is None:
            raise RuntimeError('no winner yet')
        l = len(self.decks[winner])
        return sum([(l-pos)*card for (pos, card) in enumerate(self.decks[winner])])


if __name__ == '__main__':
    c = Combat()
    c.read_input()
    c.play_game()
    print('Winning score is {0}.'.format(c.winner_score()))
