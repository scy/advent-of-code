class Ferry:

    def __init__(self):
        self.pos = [0, 0]  # east, south
        self.wp = [10, -1]  # 10 east, 1 north, according to assignment

    def input(self):
        with open('input.txt', 'r') as f:
            while True:
                line = f.readline()
                if len(line) == 0:
                    return
                line = line.strip()
                yield line[0], int(line[1:])

    def rotate_wp(self, degrees):
        if degrees not in (90, 180, 270):
            raise ValueError('invalid degree value {0}'.format(degrees))
        steps = int(degrees / 90)
        for _ in range(steps):
            self.wp = [-self.wp[1], self.wp[0]]

    def navigate(self):
        for cmd, val in self.input():
            if cmd == 'N':
                self.wp[1] -= val
            elif cmd == 'E':
                self.wp[0] += val
            elif cmd == 'S':
                self.wp[1] += val
            elif cmd == 'W':
                self.wp[0] -= val
            elif cmd == 'L':
                self.rotate_wp(360 - val)
            elif cmd == 'R':
                self.rotate_wp(val)
            elif cmd == 'F':
                self.pos[0] += val * self.wp[0]
                self.pos[1] += val * self.wp[1]
            else:
                raise NotImplementedError('unknown command {0}{1}'.format(
                    cmd, val))
            print('Ferry @ ({0},{1}), WP @ ({2},{3})'.format(
                *self.pos, *self.wp))


if __name__ == '__main__':
    f = Ferry()
    f.navigate()
    print('Manhattan distance for point ({0}, {1}) is {2}'.format(
        *f.pos, sum([abs(x) for x in f.pos])))
