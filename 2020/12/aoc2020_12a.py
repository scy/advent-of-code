class Ferry:
    DIR_TO_VEC = {
        0:   ( 1,  0),
        90:  ( 0,  1),
        180: (-1,  0),
        270: ( 0, -1),
    }

    def __init__(self):
        self.pos = [0, 0]  # east, south
        self.dir = 0  # east

    def input(self):
        with open('input.txt', 'r') as f:
            while True:
                line = f.readline()
                if len(line) == 0:
                    return
                line = line.strip()
                yield line[0], int(line[1:])

    def navigate(self):
        for cmd, val in self.input():
            if cmd == 'N':
                self.pos[1] -= val
            elif cmd == 'E':
                self.pos[0] += val
            elif cmd == 'S':
                self.pos[1] += val
            elif cmd == 'W':
                self.pos[0] -= val
            elif cmd == 'L':
                self.dir = (self.dir - val) % 360
            elif cmd == 'R':
                self.dir = (self.dir + val) % 360
            elif cmd == 'F':
                self.pos[0] += val * self.DIR_TO_VEC[self.dir][0]
                self.pos[1] += val * self.DIR_TO_VEC[self.dir][1]
            else:
                raise NotImplementedError('unknown command {0}{1}'.format(
                    cmd, val))


if __name__ == '__main__':
    f = Ferry()
    f.navigate()
    print('Manhattan distance for point ({0}, {1}) is {2}'.format(
        *f.pos, sum([abs(x) for x in f.pos])))
