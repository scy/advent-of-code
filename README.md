# scy’s Advent of Code

👇 *Nach dem ersten Abschnitt geht’s komplett auf Deutsch weiter!* 👇

## Information in English

This repository contains my solutions to some [Advent of Code](https://adventofcode.com/) puzzles.
I’m using Advent of Code to learn new programming languages or to improve my proficiency in those I already know.

Also, each year in December, when the puzzles come out, I’m doing live video streams while solving them.
That way, people can look over my shoulder, ask questions or suggest other ways to build the solution.

While the code I’m writing uses English names and comments, **the streams are in German**, my mother tongue.
This allows me to think and speak more fluently, but more importantly it provides a way for German speakers who are not very good at English to follow along.
In other words, I’m trying to lower the entry barrier for people who struggle with English.

Therefore, the documentation and announcements in this repository that deal with the streams is in German as well.
Feel free to browse the code though! 😉

You can contact me on Mastodon ([@scy@scy.name](https://mastodon.scy.name/@scy)), Twitter ([@scy](https://twitter.com/scy)), Matrix ([@scy:scy.name](https://matrix.to/#/@scy:scy.name)) or visit [the #livestream:scy.name Matrix channel](https://matrix.to/#/#livestream:scy.name).

## Worum geht’s?

Dieses Git-Repository enthält meine Lösungen zu einigen Aufgaben („Puzzles“) aus dem [Advent of Code](https://adventofcode.com/), einem jährlich stattfindenden „Programmier-Adventskalender“.
Jeden Tag im Dezember erscheinen zwei neue Aufgaben (die zweite baut auf der ersten auf), die man in einer beliebigen Programmiersprache lösen kann.
Das nehme ich gern zum Anlass, um eine neue Sprache zu lernen oder meine Kenntnisse in anderen zu vertiefen.

Im Dezember, wenn neue Puzzles rauskommen, mache ich üblicherweise Live-Videostreams, während ich sie löse.
Auf die Art können mir Leute über die Schulter schauen, Fragen stellen oder alternative Lösungswege vorschlagen.

Im Code benutze ich englische Namen für Variablen, Funktionen etc., aber **die Streams sind auf Deutsch**, meiner Muttersprache.
Dadurch kann ich flüssiger denken und reden, aber zusätzlich (noch viel wichtiger) gibt es Leuten, die nicht so gut Englisch sprechen, die Möglichkeit, mit dabei zu sein.
Ich versuche also den Einstieg für Menschen zu vereinfachen, die mit Englisch Probleme haben.

Aus diesem Grund sind in diesem Repository die Dokumentation und Ankündigungen, die sich mit den Streams befassen, auf Deutsch.

## ✨ Advent of Code 2020! ✨

Es ist Dezember; der Advent of Code fängt wieder an! 😍

2020 war und ist ein hartes Jahr für uns alle, und nachdem die Rust-Streams 2019 so anstrengend und zeitaufwendig waren, will ich’s dieses Jahr etwas relaxter.
Ich werde daher in Python coden, einer Sprache, die ich einigermaßen kenne und schätze.
Dadurch kann ich mich hoffentlich auf die Aufgaben konzentrieren, anstatt nebenher noch eine ungewohnte neue Programmiersprache zu lernen.

Damit’s nicht zu langweilig wird, gibt es zwei große Änderungen:

Erstens entwickle ich komplett in einem nicht-grafischen Linux-Terminal.
Nur Textmodus, sonst nichts.
Ich habe meinen gerade beendeten Freelance-Auftrag schon auf die Art umgesetzt und es hat erstaunlich gut funktioniert.

Zweitens lege ich einen größeren Fokus auf offene, dezentrale und unabhängige Technologien und Plattformen:

* Dieses Repository ist zu [Codeberg](https://codeberg.org/) umgezogen, einem gemeinnützigen Verein zur Förderung des Open-Source-Gedankens.
* Die Streams könnt ihr nicht nur auf YouTube sehen, sondern auch über [tmate](https://tmate.io/) im Browser bzw. über SSH (ist ja schließlich nur Text!). Details dazu findet ihr weiter unten.
* Den extrem latenzarmen Ton dazu bekommt ihr über [Mumble](https://www.mumble.info/).
* Für den Chat zum Stream benutzen wird jetzt [Matrix](https://matrix.org/) statt Discord. Wenn ihr noch keinen Account habt, könnt ihr euch [einen kostenlos auf matrix.org anlegen](https://app.element.io/). Wir sehen uns dann im Raum [#livestream:scy.name](https://matrix.to/#/#livestream:scy.name)!

### 📅 Termine und Links

* Di 2020-12-01 18:30 Uhr: Tag 1 (finde Zahlen, die in Summe 2020 ergeben) [auf YouTube](https://youtu.be/vRDvgsdasJg)
* Mi 2020-12-02 18:30 Uhr: Tag 2 (zähle „Passwörter“, die auf eine bestimmte Policy passen) [auf YouTube](https://youtu.be/0BRd6QtZvdg)
* Do 2020-12-03 18:30 Uhr: Tag 3 (zähle Bäume auf einer Linie) [auf YouTube](https://youtu.be/TkwDmkFE-iA)
* Fr 2020-12-04 18:30 Uhr: Tag 4 (Reisepässe) [auf YouTube](https://youtu.be/oTOlMThMrUw)
* Sa 2020-12-05 21:00 Uhr: Tag 5 (verlorener Boardingpass) [auf YouTube](https://youtu.be/ukKuMM-MBaE)
* So 2020-12-06 18:30 Uhr: Tag 6 (Zollformulare) [auf YouTube](https://youtu.be/SoFI8OwIKGc)
* Mo 2020-12-07 18:30 Uhr: Tag 7 (ineinander liegende Taschen) [auf YouTube](https://youtu.be/0kcfEYt95Os)
* Di 2020-12-08 18:30 Uhr: Tag 8 (fehlerhafte Instruktion in Spielkonsole) [auf YouTube](https://youtu.be/KT-dyqXCwjU)
* Mi 2020-12-09 18:30 Uhr: Tag 9 (Flugzeugverschlüsselung knacken) [auf YouTube](https://youtu.be/l18yZ5xzOwM)
* Do 2020-12-10 18:30 Uhr: Tag 10 (Steckdosenadapter permutieren) [auf YouTube](https://youtu.be/--stC_igpKY)
* Fr 2020-12-11 19:00 Uhr: Tag 11 (Sitzplatzverteilung Fähre) [auf YouTube](https://youtu.be/6toUt-FmpIc)
* Sa 2020-12-12 12:00 Uhr: Tag 12 (Schiffsnavigation) [auf YouTube](https://youtu.be/LXQpAvjLutE)
* So 2020-12-13 20:30 Uhr: Tag 13 (Bus-Abfahrtzeiten) [auf YouTube](https://youtu.be/X-R3U3CTTiQ)
* Mo 2020-12-14 18:30 Uhr: Tag 14 (Docking-Computer mit Bitmasken) [auf YouTube](https://youtu.be/OMfq0NjEuS8)
* Di 2020-12-15 20:00 Uhr: Tag 15 (Zahlen-Memory) [auf YouTube](https://youtu.be/KM1YmZkZWBY)
* Mi 2020-12-16 18:30 Uhr: Tag 16 (Mapping von Zugticket-Feldern) [auf YouTube](https://youtu.be/e-Oau8MUf0U)
* Do 2020-12-17 18:30 Uhr: Tag 17 (3D-Conway) [auf YouTube](https://youtu.be/ehmiT1sAEfY)
* Fr 2020-12-18 18:30 Uhr: Tag 18 (ungewöhnliche Operator-Reihenfolge) [auf YouTube](https://youtu.be/Bamd6VjK0tg)
* Sa 2020-12-19 19:30 Uhr: Tag 19 (rekursive Message-Regeln) [auf YouTube](https://youtu.be/baUNizuPhY0)
* So 2020-12-20 18:30 Uhr: Tag 20 (Bildfragmente stitchen) [auf YouTube](https://youtu.be/YAM5RTo4rd8) – aufgegeben! 😩
* Mo 2020-12-21 14:45 Uhr: Tag 21 (Allergene) [auf YouTube](https://youtu.be/3eo0F2jvfpU)
* Di 2020-12-22 18:30 Uhr: Tag 22 (Kartenspiel gegen 🦀) [auf YouTube](https://youtu.be/Ik1dmRFo8r8)
* Mi 2020-12-23 18:30 Uhr: Tag 23 (Hütchenspiel mit 🦀) [auf YouTube](https://youtu.be/_0QK5pjsVLI)
* Do 2020-12-24 09:00 Uhr: Tag 24 (sechseckige Bodenfliesen) [auf YouTube](https://youtu.be/YXY2LeANsWw)
* Fr 2020-12-25 18:30 Uhr: Tag 25 [auf YouTube](https://youtu.be/4zkcs_e79co)

### Konsolenstream via tmate und Mumble

Wenn ihr Google nicht mögt, die Bandbreite für nen Videostream sparen wollt, meinen Stream gern in eurer eigenen Lieblingsschriftart und -farbschema sehen würdet oder um einfach mal ne nerdige Art eines Livestreams auszuprobieren:
Ihr könnt statt über YouTube auch über tmate und Mumble zuschauen.

* Um im Browser zuzuschauen, ruft ihr <https://tmate.io/t/scy/aoc2020> auf. (Sinnvollerweise erst zur angekündigten Uhrzeit.)
* Wenn ihr stattdessen SSH benutzen wollt, ist das entsprechende Kommando `ssh scy/aoc2020@lon1.tmate.io`. Ihr landet direkt in meiner Konsole (könnt aber nicht selbst tippen). Um euch wieder auszuloggen, benutzt ihr am besten SSHs eingebaute Tastenfolge `Enter`, dann `~` (Tilde), dann `.` (Punkt). Alternativ könnt ihr natürlich auch euer Terminalfenster schließen oder euch was anderes ausdenken, `killall ssh` zum Beispiel.
* Um in Mumble zuzuhören, kommt in den [Kanal „Livestream“](mumble://mumble.scy.name/Livestream?title=scy's%20Mumble&version=1.2.0) auf meinem Server `mumble.scy.name`. Das Passwort, um dem Server beizutreten, lautet festlicherweise `Datenbraten`. Ihr könnt in diesem Kanal nur zuhören, nicht sprechen (und auch nicht chatten, dafür gibt’s Matrix).
* Bis zum 19. Dezember 2020 war's so, dass beim Verbinden via tmate eure IP-Adresse auf meinem Bildschirm angezeigt wurde und damit auch im Stream landete. Das ist nicht mehr so, eure IP bekommt also niemand zu sehen.

Eins noch:
Wenn ihr per SSH zuseht, muss euer Terminal mindestens so groß sein wie meins.
Sollte eures größer sein, füllt tmate den Rest eures Bildschirm mit Punkten auf.
Das ist normal, macht tmux auch so.

## Advent of Code 2019

2019 habe ich mich in komplett unbekanntes Gebiet begeben und die Aufgaben in [Rust](https://www.rust-lang.org/) programmiert.
Ich war totaler Newbie und hatte bis November noch keine Zeile Code darin geschrieben.
Es gab Vorab-Streams, in denen ich Rust aufgesetzt habe und [Rust by Example](https://doc.rust-lang.org/rust-by-example/) ein Stück weit durchgearbeitet:

* Mi 2019-11-20: [Bauen am Streaming-Setup](https://youtu.be/qIXtNoK_bH0)
* Do 2019-11-21: [„Rust by Example“ Teil 1: Installation, Hello World, Stringformatierung](https://youtu.be/nBgGx9z9xTA)
* Fr 2019-11-22: [„Rust by Example“ Teil 2: Primitive, Tupel, Arrays, Slices, Enums](https://youtu.be/Sb0CbKU4D5o)
* Di 2019-11-26: [pythoneer’s Rust-Tutorial: Ownership, Borrows, Move und Lifetimes](https://youtu.be/vuKHcaTEkdo)

Leider fluktuierte der Zeitaufwand massiv.
Während 2018 alles in zwei Stunden lösbar war, sind 2019 Streams mit vier Stunden oder mehr keine Seltenheit gewesen.
Den Zeitaufwand konnte ich irgendwann nicht mehr stemmen, weshalb ich die Streams nach Tag 14 abgebrochen habe.
Sorry dafür!

Wenn ihr die 2019er Streams von vorn bis hinten durchschauen wollt, gibt es hier die Playlist:
[Advent of Code 2019](https://www.youtube.com/playlist?list=PLmsG3H3Vzkf-CMt-3GY7sG8D_6pGh1cA7)

* So 2019-12-01: [Tag 1](https://youtu.be/h-Zgni6L680) (Raumschiff-Treibstoff)
* Mo 2019-12-02: [Tag 2](https://youtu.be/_DnboNZCm0s) (erstes Intcode-Programm: Addition/Multiplikation)
* Di 2019-12-03: [Tag 3](https://youtu.be/wteaJdGEu1I) (Schnittpunkte zwischen zwei Drähten)
* Mi 2019-12-04: [Tag 4](https://youtu.be/-llJ5Cq_PQM) („Passwörter“ aus Ziffern mit Gültigkeitsregeln)
* Do 2019-12-05: [Tag 5](https://youtu.be/-ISgDEmrbLs) (Intcode-Erweiterungen: Parameter-Modus „Immediate“, Jump, Compare)
* Fr 2019-12-06: [Tag 6](https://youtu.be/6JJ8ZTOlPCI) (Graphtraversierung bzw. „Orbitaltransfers“)
* Sa 2019-12-07: [Tag 7](https://youtu.be/AuqY50f1rAU) (Verkettung von Intcode-Maschinen, Warten auf Input)
* So 2019-12-08: [Tag 8](https://youtu.be/3pbzOg2QCTc) (Passwort-Bild aus teils transparenten Layern)
* Mo 2019-12-09: [Tag 9](https://youtu.be/L_rm1t1ZtOI) (Relative Adressierung und mehrere Outputs im Intcode)
* Di 2019-12-10: [Tag 10](https://youtu.be/TjcIJJKWxKU) (Asteroiden: Sichtbarkeit und Abschuss im Uhrzeigersinn)
* Mi 2019-12-11: [Tag 11](https://youtu.be/Q9Zy0lyZo7E) (Kennzeichen auf die Raumschiffhülle malen lassen)
* Do 2019-12-12: [Tag 12](https://youtu.be/SAdv45uErnM) (Umlaufbahnen der Jupitermonde)
* Fr 2019-12-13: [Tag 13](https://youtu.be/HpLr7USHiQQ) (Breakout-Klon in Intcode)
* Sa 2019-12-14: [Tag 14](https://youtu.be/hIygbzwiUe0) (Produktion von Treibstoff aus Erz über verschiedene chemische Reaktionen)

## Advent of Code 2018

2018 lag mein Fokus auf modernem JavaScript, sprich ≥ ES6.
Dafür habe ich mich mit dem hervorragenden [JavaScript.info](https://javascript.info/) eingearbeitet und dort auch immer wieder nachgeschlagen.

Leider ist am 13.12. (wann auch sonst) meine Platte abgeraucht und ich habe die Streams nicht fortgesetzt.

Die Streams der ersten zwölf Tage findet ihr in dieser YouTube-Playlist: [Advent of Code 2018](https://www.youtube.com/playlist?list=PLmsG3H3Vzkf_HZppROt70VbflbSEJtj1K), den Code natürlich hier im Repo im Ordner [2018](2018).

## FAQ

### Warum streamst du nicht auf Twitch?

Ich versuche seit ca. zwei Jahren, so wenig mit Amazon (denen ja Twitch gehört) zu tun zu haben wie möglich.
Die Arbeitsbedingungen dort sind ausbeuterisch, deren CEO Jeff Bezos hordet sein Geld lieber, anstatt damit Gutes zu tun und generell sind sie mir einfach unsympathisch.
Google ist jetzt aus antikapitalistischer Sicht nur marginal besser, aber freie oder dezentrale Streamingplattformen gibt’s eben noch nicht so wirklich.
[PeerTube](https://joinpeertube.org/) ist kurz davor und hat Livestreaming wohl (Stand 2020-12-01) schon [umgesetzt](https://github.com/Chocobozzz/PeerTube/pull/3250), aber es gibt noch kein neues Release.

### Warum benutzt du nicht den YouTube-Chat, sondern Matrix?

Mir ist klar, dass es ne signifikante Hürde darstellt, dass ich den YouTube-Chat abgeschaltet habe.
Das ist aber tatsächlich eine sehr bewusste Entscheidung:

Erstens heißt du im YouTube-Chat so wie dein „Kanal“, was üblicherweise deinem Google-Namen und damit oft deinem Passnamen entspricht.
Sich einen Nickname auszusuchen, ist nicht so einfach, außer du benennst deinen „Kanal“ um.
Und wenn du dich mit deinem Nickname identifizierst, fühlt sich der Passname einfach blöd an.
Abgesehen davon fällt’s mir schwer, dich zuzuordnen, wenn ich dich nur unter deinem Nick kenne.

Zweitens möchte ich aber auch eine Community abseits von den paar Stunden Stream hochziehen, z.B. für andere Livestreams oder gemeinsame Projekte, von daher macht’s eben einfach Sinn, nen Dienst zu benutzen, der auch abseits der Streams genutzt werden kann.

Drittens möchte ich die Streams auch dafür nutzen, Alternativen zu den großen Platzhirschen aus der Cloud zu pushen.
Und ich muss sagen, ich bin auch einfach recht überzeugt von Matrix.
Inzwischen ist es reif genug, um nicht nur was für krasse Nerds zu sein.

Abgesehen davon will ich in Zukunft auch den YouTube-Chat _zusätzlich_ noch einbinden, denn Möglichkeiten dazu gibt’s durchaus – mir hat aber die Zeit gefehlt, um das rechtzeitig zum Advent of Code 2020 fertig zu kriegen.
